<?php

namespace Tests\Feature;

use App\Jobs\CheckMonitor;
use App\Models\Monitor;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Queue;
use Tests\TestCase;

class MonitorsTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();

        $this->withHeaders([
            'Accept' => 'application/json',
        ]);
    }

    public function test_get_monitor()
    {
        $monitor = Monitor::factory()->create();

        $hostname = parse_url($monitor->url, PHP_URL_HOST);
        $response = $this->get('/api/monitors/' . $hostname);

        $response->assertStatus(200);
    }

    public function test_add_monitors()
    {
        $response = $this->postJson('/api/monitors', [
            "https://onet.pl/",
            "http://test-redirects.137.software",
            "https://www.google.com",
            "http://socialmention.com/"
        ]);

        $response->assertStatus(200);
        $response->assertHeaderMissing('X-Stats');
    }

    public function test_add_monitors_with_stats()
    {
        Queue::fake();
        $response = $this->postJson('/api/monitors?stats=true', [
            "https://onet.pl/",
        ]);

        $response->assertStatus(200);
        $response->assertHeader('X-Stats', json_encode([["https://onet.pl/" => null]]));
        Queue::assertPushed(CheckMonitor::class);
    }

    public function test_add_invalid_monitors()
    {
        Queue::fake();
        $response = $this->postJson('/api/monitors?stats=true', [
            "https://onet2.pl/",
        ]);

        $response
            ->assertStatus(200)
            ->assertJson([
                'data' => [],
                'errors' => [
                    'https://onet2.pl/ is not a valid url!',
                ],
            ]);
        $response->assertHeader('X-Stats', json_encode([]));
        Queue::assertNotPushed(CheckMonitor::class);
    }
}
