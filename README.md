## InboundWay recruitment task

* I recommend using `sails` for running and testing project
* I decide to add custom validation, since when it is possible to add many monitors at once, there is no need to fail all request when only some of them are broken
* I resigned from custom form request for validation and prepare data because of custom validation logic
* I have attached supervisor config file in root directory, but if I had to decide I would rather put it on ecs service or k8s pod, and let infrastructure keep it alive
