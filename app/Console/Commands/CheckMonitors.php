<?php

namespace App\Console\Commands;

use App\Jobs\CheckMonitor;
use App\Models\Monitor;
use Illuminate\Console\Command;

class CheckMonitors extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'monitors:check {--monitor= : Check single monitor}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check monitors';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if ($this->option('monitor')) {
            $monitors = [Monitor::findOrFail($this->option('monitor'))];
        } else {
            $monitors = Monitor::all();
        }

        foreach ($monitors as $monitor) {
            dispatch(new CheckMonitor($monitor));
        }

        return 0;
    }
}
