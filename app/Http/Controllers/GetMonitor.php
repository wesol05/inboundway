<?php

namespace App\Http\Controllers;

use App\Http\Resources\CheckResource;
use App\Models\Monitor;
use Carbon\Carbon;

class GetMonitor extends Controller
{
    public function __invoke(string $url)
    {
        $monitor = Monitor::where('url', 'like', '%' . $url . '%')->firstOrFail();


        $checks = $monitor->checks()->where('created_at', '>=', Carbon::now()->subMinutes(10))
            ->orderBy('created_at', 'desc')
            ->get();

        return CheckResource::collection($checks);
    }
}
