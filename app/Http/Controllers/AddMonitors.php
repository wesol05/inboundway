<?php

namespace App\Http\Controllers;

use App\Jobs\CheckMonitor;
use App\Models\Monitor;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

class AddMonitors extends Controller
{
    public function __invoke(Request $request)
    {
        $urls = $request->json()->all();
        $collectStats = $request->query->getBoolean('stats');

        $errors = [];
        $monitors = new Collection();

        foreach ($urls as $url) {
            $result = $this->processUrl($url, $collectStats);

            if (is_string($result)) {
                $errors[] = $result;
            }

            if ($result instanceof Monitor) {
                $monitors->add($result);
                continue;
            }
        }

        $response = response([
            'data' => [],
            'errors' => $errors,
        ]);

        if ($collectStats) {
            sleep(2);
            $response->header('X-Stats', $this->xStatsValue($monitors));
        }

        return $response;
    }

    private function processUrl(string $url, bool $collectStats): Monitor|string
    {
        $host = parse_url($url, PHP_URL_HOST);

        if (!$this->validateActiveHost($host)) {
            return sprintf('%s is not a valid url!', $url);
        }

        if (Monitor::where('url', 'like', '%' . $host . '%')->exists()) {
            return sprintf('Url with given hostname already exists: "%s"!', $url);
        }

        $monitor = Monitor::create([
            'url' => $url,
        ]);

        if ($collectStats) {
            $this->dispatch(new CheckMonitor($monitor));
        }

        return $monitor;
    }

    /** @see laravel/framework/src/Illuminate/Validation/Concerns/ValidatesAttributes.php:51 */
    private function validateActiveHost(string $host): bool
    {
        try {
            return count(dns_get_record($host, DNS_A | DNS_AAAA)) > 0;
        } catch (\Exception $e) {
            return false;
        }
    }

    private function xStatsValue(Collection $monitors): Collection|\Illuminate\Support\Collection
    {
        $monitors->fresh();

        return $monitors->map(function (Monitor $monitor) {
            $check = $monitor
                ->checks()
                ->orderBy('created_at', 'desc')
                ->first();

            return [
                $monitor->url => $check ? $check->duration : null
            ];
        });
    }
}
