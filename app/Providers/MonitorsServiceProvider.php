<?php

namespace App\Providers;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use Illuminate\Support\ServiceProvider;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\UriInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Stopwatch\Stopwatch;

class MonitorsServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Client::class, function ($app) {
            $logger = $app->get(LoggerInterface::class);
            return new Client([
                'timeout' => 30,
                'allow_redirects' => [
                    'max' => 999,
                    'strict' => false,
                    'track_redirects' => true,
                    'on_redirect' => function (
                        RequestInterface $request,
                        Response $response,
                        UriInterface $uri
                    ) use ($logger) {
                        $logger->debug(sprintf('Redirecting! %s to %s', $request->getUri(), $uri));
                    }
                ],
            ]);
        });

        $this->app->singleton(Stopwatch::class, function ($app) {
            return new Stopwatch(true);
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
