<?php

namespace App\Jobs;

use App\Models\Monitor;
use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Symfony\Component\Stopwatch\Stopwatch;

class CheckMonitor implements ShouldQueue, ShouldBeUnique
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $monitor;
    private $url;

    public function __construct(Monitor $monitor)
    {
        $this->monitor = $monitor;
        $this->url = $monitor->url;
    }

    public function handle(Client $client, Stopwatch $stopwatch)
    {
        $stopwatch->start($this->url);
        $res = $client->get($this->url);
        $stopwatch->stop($this->url);

        $redirects = count(explode(' ', $res->getHeaderLine('X-Guzzle-Redirect-History')));
        $duration = $stopwatch->getEvent($this->url)->getDuration();

        $this->monitor->checks()->create([
            'duration' => $duration,
            'redirects' => $redirects,
        ]);
    }

    public function uniqueId()
    {
        return $this->url;
    }
}
