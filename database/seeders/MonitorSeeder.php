<?php

namespace Database\Seeders;

use App\Models\Check;
use App\Models\Monitor;
use Illuminate\Database\Seeder;

class MonitorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Monitor::factory(10)
            ->has(Check::factory()
                ->count(10)
            )
            ->create();
    }
}
